
import game.Game;
import util.FileManipulation;
import util.InputDataDancingAnts;

import java.util.*;

/**
 * Main class to file
 * @author Rémi Teissier
 */
public class Main {


    public static void main(String[] args) {
        InputDataDancingAnts inputData = new InputDataDancingAnts(FileManipulation.stringFromFile(args[0]));
        Game g = new Game(inputData);
        g.run(inputData.getIteration());
        System.out.println(g.toString());
        FileManipulation.createResultFile(g.toString());
    }

}