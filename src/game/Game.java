package game;

import util.InputDataDancingAnts;

import java.util.ArrayList;

/**
 * Game class which compute a {@link Board} for n iteration
 * @author Rémi Teissier
 */
public class Game {

    /**
     * Default constructor
     * @param inputData Data of the config
     */
    public Game(InputDataDancingAnts inputData) {
        ArrayList<Ant> ants = new ArrayList<>();
        for (int[] antData : inputData.getAnts()) {
            ants.add(new Ant(antData[0], antData[1], antData[2], antData[3]!= 0));
        }
        board = new Board(ants,
                inputData.getBoard()[0],
                inputData.getBoard()[1],
                new ColorPattern(inputData.getColorPattern()),
                inputData.getTeleport()
                );
    }

    /**
     * Board to compute
     */
    private Board board;

    /**
     * Compute the board
     * @param iteration Number of iteration to compute
     */
    public void run(int iteration) {
        for (int i = 0; i < iteration; i++) {
            board.update();
        }
    }

    @Override
    public String toString() {
        return board.toString();
    }
}