package game;

/**
 * Ant object to simulate its comportment in the {@link Board} with {@link Cell}
 *
 * @author Rémi Teissier
 */
public class Ant {

    /**
     * Default constructor
     * @param i Position i in a Matrix
     * @param j Position i in a Matrix
     * @param direction Direction where the Ant will walk
     */
    public Ant(int i, int j, int direction, boolean isInverted) {
        position = new Position(i, j);
        this.direction = direction;
        this.isInverted = isInverted;
    }

    /**
     * Direction where the Ant look
     * <ul>
     *     <li><strong>0 :</strong> up</li>
     *     <li><strong>1 :</strong> right</li>
     *     <li><strong>2 :</strong> down</li>
     *     <li><strong>3 :</strong> left</li>
     * </ul>
     */
    private int direction;

    /**
     * {@link Board} where the Ant is on
     */
    private Board board;

    /**
     * Position in the Matrix of {@link Board} with a {@link Position} object
     */
    private Position position;

    /**
     * Determine if direction after passed on a cell is inverted (!colorPattern.direction)
     */
    private boolean isInverted;

    /**
     * Update a step forward the Ant on the {@link Board}
     * @return If the Ant has been out of bound
     */
    boolean update () {
        board.getCell(position).changeColor();
        boolean outOfBorder = next();
        boolean cellDirection = board.getCell(position).getDirection();

        if (isInverted != cellDirection) {
            right();
        } else {
            left();
        }
        return outOfBorder;
    }

    /**
     * Look at the left from the original direction
     */
    private void left() {
        direction--;
        if (direction<0) {
            direction = 3;
        } else if (direction>3) {
            direction = 0;
        }
    }

    /**
     * Look at the right from the original direction
     */
    private void right() {
        direction++;
        if (direction>3) {
            direction = 0;
        } else if (direction<0) {
            direction = 3;
        }
    }

    /**
     * Step forward in the Matrix according to the {@link this.position}
     * @return If the Ant has been out of border
     */
    private boolean next() {
        switch (direction) {
            case 0:
                position.up();
                break;
            case 1:
                position.right();
                break;
            case 2:
                position.down();
                break;
            case 3:
                position.left();
                break;
        }
        boolean outOfBorder = false;

        if (position.getI() < 0) {
            outOfBorder = true;
            position.setI(board.getHeight() - 1);
        } else if (board.getHeight() <= position.getI()) {
            outOfBorder = true;
            position.setI(0);
        } else if (position.getJ() < 0) {
            outOfBorder = true;
            position.setJ(board.getWidth() - 1);
        } else if (board.getWidth() <= position.getJ()) {
            outOfBorder = true;
            position.setJ(0);
        }
        return outOfBorder;
    }

    /**
     * @return {@link Position} object of the Ant
     */
    Position getPosition() {
        return position;
    }

    /**
     *Set the board where the ant move
     * @param board Board of cells
     */
    void setBoard (Board board) {
        this.board = board;
    }
}