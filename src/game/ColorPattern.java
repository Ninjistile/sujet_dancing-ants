package game;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Color Pattern for a Cell, which determine the direction of the ant
 * @author Rémi Teissier
 */
public class ColorPattern {

    /**
     * Default constructor
     * @param directions Directions in order
     */
    public ColorPattern(boolean[] directions) {
        this.directions = directions;
    }

    /**
     * Default constructor
     * @param directions Directions in order
     */
    public ColorPattern(ArrayList<Boolean> directions) {
        this.directions = new boolean[directions.size()];
        for (int i = 0; i < this.directions.length; i++) {
            this.directions[i] = directions.get(i);
        }
    }

    /**
     * Direction in order :
     * <ul>
     *     <li><strong>true :</strong> Right</li>
     *     <li><strong>false :</strong> Left</li>
     * </ul>
     */
    private boolean[] directions;

    /**
     * Get next index of the actual color
     * @param actual Actual index of the cell
     * @return New index
     */
    public int getNext(int actual) {
        actual++;
        actual = (actual >= directions.length)? 0 : actual;
        return actual;
    }

    /**
     * Get the boolean direction of the index i
     * @param i Index of the direction
     * @return Boolean direction
     */
    public boolean getDirection(int i) {
        return directions[i];
    }

}