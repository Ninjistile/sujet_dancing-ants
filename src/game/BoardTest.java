package game;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class BoardTest {

    @Test
    public void test1Ant () {
        ColorPattern colorPattern = new ColorPattern(new boolean[] {true, false});
        ArrayList<Ant> ants = new ArrayList<>();
        ants.add(new Ant(5, 5, 1, false));
        Board board = new Board(ants, 10, 10, colorPattern, false);
        for (int i = 0; i < 5; i++) {
            board.update();
        }
        assertEquals(5, ants.get(0).getPosition().getI());
        assertEquals(4, ants.get(0).getPosition().getJ());
    }

    @Test
    public void test1AntTp () {
        ColorPattern colorPattern = new ColorPattern(new boolean[] {true, false});
        ArrayList<Ant> ants = new ArrayList<>();
        ants.add(new Ant(0, 0, 1, false));
        Board board = new Board(ants, 2, 2, colorPattern, true);
        for (int i = 0; i < 5; i++) {
            board.update();
        }
        assertEquals(0, ants.get(0).getPosition().getI());
        assertEquals(1, ants.get(0).getPosition().getJ());
    }

    @Test
    public void test2AntMultiColor () {
        ColorPattern colorPattern = new ColorPattern(new boolean[] {true, false, true});
        ArrayList<Ant> ants = new ArrayList<>();
        ants.add(new Ant(5, 5, 1, false));
        ants.add(new Ant(5, 5, 1, false));
        Board board = new Board(ants, 10, 10, colorPattern, true);
        for (int i = 0; i < 5; i++) {
            board.update();
        }
        assertEquals(5, ants.get(0).getPosition().getI());
        assertEquals(6, ants.get(0).getPosition().getJ());
        assertEquals(5, ants.get(1).getPosition().getI());
        assertEquals(6, ants.get(1).getPosition().getJ());
    }
}
