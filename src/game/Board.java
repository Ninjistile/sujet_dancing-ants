package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Board of cells with Ants operating on it
 *
 * @author Rémi Teissier
 */
public class Board {

    /**
     * Default constructor
     * @param ants List of {@link Ant}
     * @param width Width of the board
     * @param height Height of the board
     * @param colorPattern Color Pattern to respect for each {@link Cell}
     * @param teleportAnt If Ants teleport or die out of
     */
    public Board(List<Ant> ants, int width, int height, ColorPattern colorPattern, boolean teleportAnt) {
        this.ants = ants;
        for (Ant ant : this.ants) {
            ant.setBoard(this);
        }
        this.teleportAnt = teleportAnt;
        this.cells = new Cell[height][width];
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                cells[i][j] = new Cell(colorPattern);
            }
        }
    }

    /**
     * If Ants teleport if out of bound or die
     */
    private boolean teleportAnt;

    /**
     * Array of ants to actualise
     */
    private List<Ant> ants;

    /**
     * 2D Matrix of cell
     */
    private Cell[][] cells;

    /**
     * @return The width of the Matrix cells
     */
    int getWidth () {
        return cells[0].length;
    }

    /**
     * @return The height of the Matrix cells
     */
    int getHeight () {
        return cells.length;
    }

    /**
     * @param i Row index in the Matrix
     * @param j Column index in the Matrix
     * @return {@link Cell} Of the given position by i and j
     */
    Cell getCell (int i, int j) {
        return cells[i][j];
    }

    /**
     * @param position Position of the cell
     * @return {@link Cell} Of the given {@link Position}
     */
    Cell getCell (Position position) {
        return getCell(position.getI(), position.getJ());
    }

    /**
     * Update the board
     */
    void update() {
        // List to avoid to update all cell of the Matrix
        ArrayList<Cell> cellToUpdate = new ArrayList<>();
        ArrayList<Ant> antsToRemove = new ArrayList<>();
        for (Ant ant : ants) {
            cellToUpdate.add(getCell(ant.getPosition()));
            boolean outOfBorder = ant.update();
            if (!teleportAnt && outOfBorder) {
                antsToRemove.add(ant);
            }
        }
        for (Ant antToRemove :
                antsToRemove) {
            ants.remove(antToRemove);
        }
        for (Cell cell : cellToUpdate) {
            cell.update();
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i=0; i<cells.length; i++) {
            for (int j=0; j<cells[0].length; j++) {
                boolean isAnt = false;
                for (Ant ant : ants) {
                    if (ant.getPosition().getI() == i
                        && ant.getPosition().getJ() == j) {
                        isAnt = true;
                        break;
                    }
                }
                str.append((isAnt) ? "+" : cells[i][j].toString());
            }
            str.append(";\n");

        }
        return str.toString();
    }
}