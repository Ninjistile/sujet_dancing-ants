package game;

/**
 * Position class to determine position in a 2d Matrix
 *
 * @author Rémi Teissier
 */
public class Position {

    /**
     * Default constructor
     * @param i Position i in a matrix
     * @param j Position i in a matrix
     */
    public Position(int i, int j) {
        setI(i);
        setJ(j);
    }

    /**
     * i Position i in a matrix
     */
    private int i;

    /**
     * j Position j in a matrix
     */
    private int j;

    /**
     * @param i Position i in a matrix
     */
    void setI (int i) {
        this.i = i;
    }

    /**
     * @param j Position j in a matrix
     */
    void setJ (int j) {
        this.j = j;
    }

    /**
     * @return i
     */
    int getI() {
        return i;
    }

    /**
     * @return j
     */
    int getJ() {
        return j;
    }

    /**
     * Position a step at left in the Matrix
     */
    void left() {
        j--;
    }

    /**
     * Position a step at right in the Matrix
     */
    void right() {
        j++;
    }

    /**
     * Position a step at down in the Matrix
     */
    void down() {
        i++;
    }

    /**
     * Position a step at up in the Matrix
     */
    void up() {
        i--;
    }

}