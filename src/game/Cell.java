package game;

/**
 * Cell object where the ant walk on
 *
 * @author Rémi Teissier
 */
public class Cell {

    /**
     * Default constructor
     */
    public Cell(ColorPattern colorPattern) {
        this.colorPattern = colorPattern;
    }

    /**
     * Index of the color in the {@link ColorPattern}
     */
    private int colorNow = 0;

    /**
     * Index of the next color in the {@link ColorPattern}
     */
    private int colorNext = 0;

    /**
     * Color pattern to respect
     */
    private ColorPattern colorPattern;

    /**
     * update the color with the new one
     */
    void update () {
        colorNow = colorNext;
    }

    /**
     * Change colorNext into the next one
     */
    void changeColor () {
        colorNext = colorPattern.getNext(colorNext);
    }

    /**
     * Get the direction associate with the color of the cell
     * @return The direction
     */
    boolean getDirection () {
        return colorPattern.getDirection(colorNow);
    }

    @Override
    public String toString() {
        return String.valueOf(colorNow);
    }
}