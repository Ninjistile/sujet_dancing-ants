package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.*;


/**
 * Class to write and read from files
 * @author Rémi Teissier
 */
public final class FileManipulation {

    /**
     * Return the content of a file in a String from its path
     * @param path Path of the file
     * @return String content of the file
     */
    public static String stringFromFile(String path) {
        byte[] encoded = {};
        try {
            encoded = Files.readAllBytes(Paths.get(path));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(encoded, StandardCharsets.UTF_8);
    }

    /**
     * Take a String and put it in a file. Replace the file if exist
     * @param stringContent String to print
     * @param path Path of the file to create
     */
    private static void printStringInFile(String stringContent, String path) {
        BufferedWriter writer = null;
        try {
            //create a temporary file
            File file = new File(path);

            // This will output the full path where the file will be written to...
            System.out.println(file.getCanonicalPath());

            writer = new BufferedWriter(new FileWriter(path));
            writer.write(stringContent);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Take a String and put it in the result file
     * @param stringContent String to print
     */
    public static void createResultFile (String stringContent) {
        printStringInFile(stringContent,
                new File("").getAbsolutePath()
                        + File.separator + "result.txt");
    }



    /**
     * Purify String, remove comments and split with separator ';'
     * @param string String to purify
     * @return Array string separate
     */
    static String[][] purifyStringConfig (String string) {
        // Remove comments with "// ..."
        string = string.replace("//.*$", "");

        /*Pattern patternComment = Pattern.compile("//.*\n");
        Matcher matcherComment = patternComment.matcher(string);
        string = matcherComment.replaceAll("");
        /*

         */
        /*
        for (boolean c=false; c; c=matcherComment.find()) {
        }
        */
        // Remove new lines
        string = string.replace("\n", "");
        string = string.replace("\r", "");
        // Remove white Spaces
        string = string.replace(" ", "");

        // Separate variable with ";"
        String[] array = string.split(";");

        // Separate variable and value with ":"
        String[][] array2 = new String[array.length][2];
        for (int i = 0; i < array.length; i++) {
            array2[i] = array[i].split(":");
        }
        return array2;
    }


}