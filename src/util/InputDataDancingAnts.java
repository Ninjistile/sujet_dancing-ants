package util;

import java.util.*;

/**
 * All the configuration for a run
 */
public class InputDataDancingAnts {

    /**
     * Constructor which parse the String
     * @param string String t parse
     */
    public InputDataDancingAnts(String string) {
        for (String[] array : FileManipulation.purifyStringConfig(string)) {
            switch (array[0]) {
                case "teleport":
                    if (array[1].equals("0")) {
                        teleport = false;
                    } else if (array[1].equals("1")){
                        teleport = true;
                    }
                    break;
                case "colorPattern":
                    String[] pattern = array[1].split(",");
                    for (String direction : pattern) {
                        if (direction.equals("right")) {
                            colorPattern.add(true);
                        } else {
                            colorPattern.add(false);
                        }
                    }
                    break;
                case "ant":
                    String[] antInfo = array[1].split(",");
                    int[] ant = new int[4];
                    for (int i = 0; i < 4; i++) {
                        ant[i] = Integer.valueOf(antInfo[i]);
                    }
                    ants.add(ant);
                    break;
                case "board":
                    String[] boardInfo = array[1].split(",");
                    board[0] = Integer.valueOf(boardInfo[0]);
                    board[1] = Integer.valueOf(boardInfo[1]);
                    break;
                case "iteration":
                    iteration = Integer.valueOf(array[1]);
            }
        }
    }

    /**
     * List of ants with all options in it like this :
     * <ol>
     *     <li>I position in Matrix</li>
     *     <li>J position in Matrix</li>
     *     <li>
     *         Direction :
     *         <ul><strong>0</strong> : Up</ul>
     *         <ul><strong>1</strong> : Right</ul>
     *         <ul><strong>0</strong> : Down</ul>
     *         <ul><strong>0</strong> : Left</ul>
     *     </li>
     *     <li>
     *         If Ants invert normal direction on a case (On right, will look at left...) :
     *         <ul><strong>0</strong> : Non</ul>
     *         <ul><strong>1</strong> : Oui</ul>
     *     </li>
     * </ol>
     */
    private ArrayList<int[]> ants = new ArrayList<>();

    /**
     * Colors for the color pattern :
     * <ul>
     *     <li><strong>false</strong> : Left</li>
     *     <li><strong>true</strong> : Right</li>
     * </ul>
     */
    private ArrayList<Boolean> colorPattern = new ArrayList<>();

    /**
     * {Width, height} of the board
     */
    private int[] board = new int[2];

    /**
     * If Ants teleport at border
     */
    private boolean teleport;

    /**
     * Number of iteration to do
     */
    private int iteration;


    /**
     * @return List of information about ants
     */
    public ArrayList<int[]> getAnts() {
        return ants;
    }

    /**
     * @return Color pattern
     */
    public ArrayList<Boolean> getColorPattern() {
        return colorPattern;
    }

    /**
     * @return Size of the Board
     */
    public int[] getBoard() {
        return board;
    }

    /**
     * @return If ants teleport out of border
     */
    public boolean getTeleport() {
        return teleport;
    }

    /**
     * @return Iteration to proceed
     */
    public int getIteration () {
        return iteration;
    }

}