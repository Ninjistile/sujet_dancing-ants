package util;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileManipulationTest {

    @Test
    public void stringFromFileTest () {
        assertEquals("mdr",

                FileManipulation.stringFromFile("D:\\Doc\\Documents\\workspace\\Dancing-Ants\\src\\util\\test.txt"));
    }

    @Test
    public void createResultFile () {
        FileManipulation.createResultFile("lol");
        assertEquals("lol",
                FileManipulation.stringFromFile("D:\\Doc\\Documents\\workspace\\Dancing-Ants\\result.txt"));
    }

    @Test
    public void purifyString () {
        String str =
                "// Bonobo here \n ant:lol;//mdr\nblablabla: mdr";

        String[][] toTest = FileManipulation.purifyStringConfig(str);
        String[][] expected = new String[][] {{"ant", "lol"}, {"blablabla", "mdr"}};
        Assert.assertArrayEquals(expected, toTest);
    }
}
